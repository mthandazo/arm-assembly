.section .data
string:
    .asciz "Hello, World!"

.section .text
.globl _start

_start:
    mov x0, 1                   // File descriptor is stdout
    ldr x1, =string             // Load the address of the string
    mov x2, 13                  // the length of the string 13
    mov x8, 64                  // System call number for write is 64
    svc 0                       // Make the system call

    mov x8, 93                  // System call number for exit is 93
    mov x0, 0                   // Exit code 0
    svc 0
